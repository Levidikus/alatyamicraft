package net.alatyami.alatyamicraft;

import net.alatyami.alatyamicraft.block.BlockGlowstoneInfusedGlass;
import net.alatyami.alatyamicraft.block.BlockGlowstoneInfusedGlassColored;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = AlatyamiCraftInfo.MODID, name = AlatyamiCraftInfo.NAME, version = AlatyamiCraftInfo.VERSION)
public class AlatyamiCraft {
	
	@SidedProxy(clientSide = "net.alatyami.alatyamicraft.ClientProxy", serverSide = "net.alatyami.alatyamicraft.ServerProxy")	
	public static ServerProxy proxy;

	public static Block blockGlowstoneInfusedGlass;
	public static Block blockGlowstoneInfusedGlassColored;
	
	/**
	 * Load before
	 * @param event
	 */
	@EventHandler 
	public void preInit(FMLPreInitializationEvent event) {
		blockGlowstoneInfusedGlass = new BlockGlowstoneInfusedGlass(Material.glass);
		blockGlowstoneInfusedGlassColored = new BlockGlowstoneInfusedGlassColored();
		
		this.proxy.preInit(event);
	}
	
	/**
	 * Loads during
	 * @param event
	 */
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	// Register the recipe for the Glowstone Infused blocks
    	blockGlowstoneInfusedGlass.registerBlocks();
    	
    	GameRegistry.registerBlock(metablock = new MetaBlock("metablock", Material.cloth), ItemBlockMetaBlock.class "metablock");
    	
    	//GameRegistry.addShapelessRecipe(new ItemStack(blockGlowstoneInfusedGlass, 4), new Object[]{ Blocks.glass, Blocks.glowstone });
    	this.proxy.init(event);
    }
    
    /**
     * Loads after
     * @param event
     */
	@EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		// get a list of all the blocks that are loaded in the game...
		this.proxy.postInit(event);
	}
	
	/**
	 * Define the tab for creative users
	 */
	public static CreativeTabs tabAlatyamiCraft = new CreativeTabs("tabAlatyamiCraft") {
		@Override
		public Item getTabIconItem() {
			return new ItemStack(Blocks.glowstone).getItem();
		}
		
	};
    
}
