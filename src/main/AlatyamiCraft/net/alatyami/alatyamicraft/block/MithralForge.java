package net.alatyami.alatyamicraft.block;

import net.alatyami.alatyamicraft.AlatyamiCraftInfo;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;

public class MithralForge extends Block {
	
	public IIcon Bottom;
	public IIcon Top;
	public IIcon Front;
	public IIcon Back;
	public IIcon Left;
	public IIcon Right;
	
	protected MithralForge(Material material) {
		super(material);
	}
	
	public void registerBlockIcons(IIconRegister icon) {
		Bottom = icon.registerIcon(AlatyamiCraftInfo.MODID + ":MithralForgeBottom");
		Top = icon.registerIcon(AlatyamiCraftInfo.MODID + ":MithralForgeTop");
		Front = icon.registerIcon(AlatyamiCraftInfo.MODID + ":MithralForgeFront");
		Back = icon.registerIcon(AlatyamiCraftInfo.MODID + ":MithralForgeBack");
		Left = icon.registerIcon(AlatyamiCraftInfo.MODID + ":MithralForgeLeft");
		Right = icon.registerIcon(AlatyamiCraftInfo.MODID + ":MithralForgeRight");
	}
	
	public IIcon getIcon(int side, int meta) {
		if ( side == 0 ) {
			return Bottom;
		} else if ( side == 1 ) {
			return Top;
		} else if ( side == 2 ) {
			return Front;
		} else if ( side == 3 ) {
			return Back;
		} else if ( side == 4 ) {
			return Left;
		} else if ( side == 5 ) {
			return Right;
		}
		
		// Make the default set to Side0, which should never be hit.
		return Front;
	}
}
