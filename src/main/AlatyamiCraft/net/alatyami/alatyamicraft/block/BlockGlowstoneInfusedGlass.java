package net.alatyami.alatyamicraft.block;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class BlockGlowstoneInfusedGlass extends Block {
	
	public static Block blockGlowstoneInfusedGlass;

	public IIcon[] icons = new IIcon[6];
	
	public BlockGlowstoneInfusedGlass(Material material) {
		super(material);

		// Give it light
		this.setLightLevel(0.9F);
		// Give it a sound
		this.setStepSound(this.soundTypeGlass);
		// Set its hardness
		this.setHardness(0.2F);
		
		this.setBlockName("BlockGlowstoneInfusedGlass");
		this.setBlockTextureName("alatyamicraft:blockGlowstoneInfusedGlass");
		this.setCreativeTab(CreativeTabs.tabMisc);
	}
		
	public static void initializeBlock() {
		
	}
	
	public static void registerBlock() {
		GameRegistry.registerBlock(blockGlowstoneInfusedGlass, blockGlowstoneInfusedGlass.getUnlocalizedName().substring(5));
		GameRegistry.addShapelessRecipe(new ItemStack(blockGlowstoneInfusedGlass, 1), new Object[]{ Blocks.glass, Items.glowstone_dust });
	}
	
	@Override
	public void registerBlockIcons(IIconRegister reg) {
		for ( int i = 0; i < 6; i++ ) {
			this.icons[i] = reg.registerIcon(this.textureName);
		}
	}
	
	@Override
	public IIcon getIcon(int side, int meta) {
		return this.icons[side];
	}
	
	@Override
	public boolean isOpaqueCube() {
		// glass = true
		return false;
	}
	

}
