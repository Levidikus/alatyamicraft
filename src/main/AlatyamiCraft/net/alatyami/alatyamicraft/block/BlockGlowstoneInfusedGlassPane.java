package net.alatyami.alatyamicraft.block;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockGlowstoneInfusedGlassPane extends Block {
	public static Block blockGlowstoneInfusedGlassPane;
	
	/**
	 * Constructor
	 * @param material
	 */
	public BlockGlowstoneInfusedGlassPane(Material material) {
		super(material);
		
		// Give it light, just shy of actual glowstone
		this.setLightLevel(0.9F);
		
		// Give it a sound
		this.setStepSound(this.soundTypeGlass);
		
		// Set its hardness
		this.setHardness(0.2F);
		
	}
	
	public void initializeBlock() {
		blockGlowstoneInfusedGlassPane = new BlockGlowstoneInfusedGlassPane(Material.glass).setBlockName("blockGlowstoneInfusedGlassPane");
	}
	
	public void registerBlock() {
		
	}
	
	@Override
	public boolean isOpaqueCube() {
		return false;
	}
}
