package net.alatyami.alatyamicraft.block;

import java.util.List;

import net.alatyami.alatyamicraft.AlatyamiCraft;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;

public class BlockGlowstoneInfusedGlassMetaBlock extends Block {

	public BlockGlowstoneInfusedGlassMetaBlock(String unlocalizedName, Material material) {
		super(material);
		
		this.setBlockName(unlocalizedName);
		this.setCreativeTab(CreativeTabs.tabMisc);
		this.setHardness(0.2F);
		this.setStepSound(soundTypeGlass);
		
	}
	
	@Override
	public void registerBlockIcons(IIconRegister reg) {
		
	}
	
	/**
	 * 
	 */
	@Override
	public IIcon getIcon(int side, int meta) {
		if ( meta > 5 ) meta = 0;
		// TODO: Fix the metadata block icon return.
		return AlatyamiCraft.blockGlowstoneInfusedGlass.getIcon(meta, 0);
	}

	/**
	 * damageDropped(int oldMeta) gets called when the block gets destroyed. It 
	 * returns the metadata of the dropped item based on the old metadata of the 
	 * block. Normally it just returns zero so we need to change it in order to 
	 * return the old metadata without modifying the value.
	 */
	@Override
	public int damageDropped(int meta) {
		return meta;
	}
	
	/**
	 * We need to add the meta blocks to the creativeTab.
	 */
	@Override
	public void getSubBlocks(Item item, CreativeTabs tab, List list) {
		for (int i = 0; i < 6; i++) {
			list.add(new ItemStack(item, 1, i));
		}
	}
	
}
